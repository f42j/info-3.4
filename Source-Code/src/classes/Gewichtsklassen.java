package classes;

public class Gewichtsklassen {
    //limits
    //final kann hier ignoriert werden und ist nur aus Stilgründen um Konstanten zu definieren
    public final double MINIFLIEGENGEWICHT=47.627;
    public final double HALBFLIEGENGEWICHT=48.988;
    public final double FLIEGENGEWICHT=50.802;
    public final double SUPERFLIEGENGEWICHT=52.163;
    public final double BANTAMGEWICHT=53.525;

    public double[] outputMinmax(String group) {
        switch (group) {
            case "Minifliegengewicht":
                return new double[]{0,MINIFLIEGENGEWICHT};
            case "Halbfliegengewicht":
                return new double[]{MINIFLIEGENGEWICHT+0.001,HALBFLIEGENGEWICHT};
            case "Fliegengewicht":
                return new double[]{HALBFLIEGENGEWICHT+0.001,FLIEGENGEWICHT};
            case "Super-Fliegengewicht":
                return new double[]{FLIEGENGEWICHT+0.001,SUPERFLIEGENGEWICHT};
            case "Bantamgewicht":
                return new double[]{SUPERFLIEGENGEWICHT+0.001,BANTAMGEWICHT};
        }
        return null;
    }
}

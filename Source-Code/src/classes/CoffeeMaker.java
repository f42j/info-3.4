package classes;

public class CoffeeMaker {
    private boolean working=false;
    private boolean hasCoffee=false;

    public void outputCup() {
        if (!working) {
            if (hasCoffee) {
                System.out.println("Cup of coffee");
            } else {
                while (!hasCoffee) {}
                System.out.println("Cup of coffee");
            }
        }
    }
}

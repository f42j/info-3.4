package classes;

public class Mixer {
    private boolean on = false;

    public void toggle() {
        if (on) {
            on = false;
        } else {
            on = true;
        }
    }
}
